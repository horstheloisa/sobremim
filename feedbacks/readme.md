## Feedbacks recebidos dos colegas de trabalho: 

<hr>
<br>



- Trabalhar com a Helo é fantástico, pois conhece muito da regra de negócio e sabe também da parte técnica, facilitando o desenvolvimento em muitos casos já reportados.
Ela passa confiança no de correr de um determinado ciclo, fazendo com que tenhamos mais calma para que possamos entregar algo com mais qualidade no tempo hábil. Muito bom trabalhar ao seu lado.

<hr>

- Heloisa realiza seus testes com muita qualidade, sempre disposta a ajudar e sempre participa das reuniões, agregando na conversa e propondo soluções.

<hr>

- Sempre disposta a ajudar no que for preciso além de ser ponto de apoio em relação ao teste manual e automatizado.

<hr>

- Em toda demanda que a Heloísa se propõe a testar, ela procura aprofundar em todos os detalhes das regras de negócio, o que lhe proporciona um embasamento maior ao fazer os testes, consequentemente uma entrega com maior qualidade.

<hr>

- Helo tem um vasto conhecimento tantos nos testes manuais do sistema como nos testes automatizados, tem grande domínio das ferramentas e facilidade no entendimento das demandas. Traz muito valor para o time pois traz feedbacks importantes e de grande relevância quando necessário. Consegue orientar a equipe de forma clara e direta, com total autonomia recorrendo à gestão se necessário. Tem uma visão estratégica ótima, sempre considerando prazos mas focando muito na qualidade para entregar os melhores resultados para o cliente. É muito curiosa e disposta a aprender cada vez mais, busca através de documentações formas ágeis para entregar uma solução ao encontrar algum problema. Ela respeita todos da equipe, reconhece e celebra as conquistas de cada um.

<hr>

- A Helô é bem criteriosa no dia a dia, e sempre exige a melhor entrega em todas as demandas. Entendo que ela se fruste as vezes por ter que fazer entregas não muito satisfatórias por forças maiores em alguns casos, porem mesmo diante da frustração ela entrega. Atua não somente nos testes das Storys, indo um pouco além e auxiliando na organização e avaliação de demandas de forma espontânea.

<hr>

- Sempre disposta, boas ideias, sempre quando pode está disponível e competente no que faz.

<hr>

- Excelente profissional! Muito detalhista, organizada e crítica com as demandas e sempre disposta a ajudar!

<hr>

## Pontos de desenvolvimento relatados pelos colegas de trabalho: 

<hr>

- Sinto que as vezes ela se prende a detalhes pequenos(possa ser que seja por conta de ser um tester mesmo).

<hr>

- Considerando as responsabilidades de seu cargo atual, acredito que ela supera totalmente as expectativas.

<hr>

- O único ponto de desenvolvimento que vejo para a Helô é a promoção para Anl. Quality Assurance III.



